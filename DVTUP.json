{
  "swagger": "2.0",
  "info": {
    "description": "This is the API definition of Data Valuation Tool from the User Perspective in PIMCITY project. Security issues still to be discussed and added to the endpoints.",
    "version": "1.0.0",
    "title": "DVTUP API",
    "termsOfService": "This API is intended for internal use by the data trading engine, and it will not be publicly exposed",
    "contact": {
      "email": "santiago.azcoitia@imdea.org"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "TBD",
  "basePath": "/dvtup",
  "tags": [
    {
      "name": "accuracy",
      "description": "Get the accuracy of a model for a set of users",
      "externalDocs": {
        "description": "PIMCITY DVTUP design document",
        "url": "https://networks.imdea.org/"
      }
    },
    {
      "name": "value",
      "description": "Trigger valuation operations and get results",
      "externalDocs": {
        "description": "PIMCITY DVTUP design document",
        "url": "https://networks.imdea.org/"
      }
    }
  ],
  "schemes": [
    "https",
    "http"
  ],
  "paths": {
    "/accuracy": {
      "post": {
        "tags": [
          "accuracy"
        ],
        "summary": "Gets the accuracy achieved in the specific model by data from a specific set of users.",
        "description": "It starts an execution of a model for a certain set of PIMS users, and responds with the accuracy achieved by the specific model using their data from the specific set of users in the PIMS. Initially, models will be identifiable by an integerm and the list of available models will be restricted to those supported in PIMCITY demo.",
        "operationId": "getAccuracy",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "JSON object with the following properties: (1) Transaction ID, (2) Model ID, and (3) Set of users S.",
            "required": true,
            "schema": {
              "$ref": "#/definitions/getAccuracyOrder"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "number"
            }
          },
          "400": {
            "description": "Invalid model"
          },
          "401": {
            "description": "Invalid user"
          }
        }
      }
    },
    "/value": {
      "post": {
        "tags": [
          "value"
        ],
        "summary": "Trigger valuation work",
        "description": "It triggers a work to determine the relative accuracy achieved in a specific model by each data source or aggregation of data sources by using the methodology stated in the parameter.",
        "operationId": "triggerValuation",
        "consumes": [
          "application/json",
          "application/xml"
        ],
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "JSON object with the following properties: (1) Transaction ID, (2) Model ID, (3) Set of users S, (4) Method to be used to calculate the relative value, (5) Truncation value, (6) Depth parameter, and (7) Stop condition",
            "required": true,
            "schema": {
              "$ref": "#/definitions/triggerValueOrder"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK. Valuation task triggered"
          },
          "400": {
            "description": "Invalid model"
          },
          "401": {
            "description": "Invalid user"
          }
        }
      },
      "get": {
        "tags": [
          "value"
        ],
        "summary": "Get the result of a valuation task",
        "description": "Polls the server for results of a valuation task {value_id}. In case the task is over, it responds with the relative accuracy achieved in the specific valuation task by each data source or aggregation of data sources.",
        "operationId": "getValue",
        "produces": [
          "application/xml",
          "application/json"
        ],
        "parameters": [
          {
            "name": "value_id",
            "in": "query",
            "description": "Valuation task id whose results the user wants to get",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/valueResponse"
              }
            }
          },
          "400": {
            "description": "Invalid status value"
          }
        }
      }
    }
  },
  "definitions": {
    "getAccuracyOrder": {
      "type": "object",
      "properties": {
        "transaction_id": {
          "type": "integer",
          "format": "int64"
        },
        "model_id": {
          "type": "integer",
          "format": "int32"
        },
        "users": {
          "type": "array",
          "xml": {
            "name": "userList",
            "wrapped": true
          },
          "items": {
            "type": "integer"
          }
        }
      },
      "xml": {
        "name": "getAccuracyOrder"
      }
    },
    "triggerValueOrder": {
      "type": "object",
      "properties": {
        "transaction_id": {
          "type": "integer",
          "format": "int64"
        },
        "model_id": {
          "type": "integer",
          "format": "int32"
        },
        "users": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "int64"
          }
        },
        "method": {
          "type": "integer",
          "format": "int32"
        },
        "truncation_value": {
          "type": "number",
          "format": "double"
        },
        "depth": {
          "type": "integer"
        },
        "stop_condition": {
          "type": "number"
        }
      },
      "xml": {
        "name": "getAccuracyOrder"
      }
    },
    "valueResponse": {
      "type": "object",
      "properties": {
        "user_id": {
          "type": "integer",
          "format": "int64"
        },
        "value": {
          "type": "number",
          "format": "double"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Swagger",
    "url": "http://swagger.io"
  }
}