# -*- coding: utf-8 -*-
"""
Created on Jan 2021

@author: santi
"""


""" 

Class DataCombination

Abstract model class including the methods that need to be defined to combine
different data sources into one specific input y to be introduced to a model
or algorithm.

"""
class vbDataCombination:
    
    
    # Get a combined input for the set K of sources
    def getCombinedInput(self, K): pass

    # Gets the set of sources
    def getSources(self): pass


""" 

Class Model

Abstract model class including the methods that need to be defined to calculate
the output from the inputs defined by the DataCombination module.

"""
class vbModel:
    def getModelOutput(self, y): pass

""" 

Class ValueFunction

Abstract model class including the methods that need to be defined to evaluate
the output from a model. Typically, it will be a similarity metric that maps


"""
class vbValueFunction:
    """  Attributes: 
            defaultValue, which is the value which will be used in case
        of any failure either in the model fitting or in the evaluation of the
        result from the model.
            yTest, which is the object against which the class compares the
        output of the model
    """
    
    
    def __init__ (self, yTest, i_defaultValue = 0):
        self.defaultValue = i_defaultValue
    
    def evaluateOutput(self, y1): pass